import 'package:get/state_manager.dart';

class Person {
  String name;
  int age;
  String address;

  Person({this.name, this.age, this.address});

  Person.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    age = json['age'];
    address = json['address'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['age'] = this.age;
    data['address'] = this.address;
    return data;
  }
}