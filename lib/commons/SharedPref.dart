import 'package:get_storage/get_storage.dart';

class SharedPref{
  static save(String key,dynamic value){
    GetStorage().write(key, value);
  }

  static read(String key){
    return GetStorage().read(key);
  }

  static remove(String key){
    return GetStorage().remove(key);
  }
}