import 'package:base_getx/model/LanguageModel.dart';

class AppConstant{
  static String BASE_URL="";

  static languages(){
    List<LanguageModel> list=List();
    list.add(LanguageModel(language:'English',symbol:'en_US'));
    list.add(LanguageModel(language:'VietNam',symbol:'vi'));
    list.add(LanguageModel(language:'China',symbol:'cn'));
    return list;
  }
}