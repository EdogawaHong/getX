import 'dart:io';

import 'package:base_getx/commons/AppConstant.dart';
import 'package:base_getx/commons/Utils.dart';
import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import '../Constance.dart';
import '../SharedPref.dart';

class NetworkUtils {
  static Future<String> getTOKEN() async {
    return await SharedPref.read(Constance.Token);
  }

  static String getBaseUrl() {
    return AppConstant.BASE_URL;
  }

  static Dio getDioClientToken() {
    String token = Constance.TokenKey;
    getTOKEN().then((tokenSaved) {
      token = tokenSaved;
    });
    print("Token Request = $token");
    final dio = Dio(BaseOptions(
        baseUrl: getBaseUrl(),
        contentType: "application/json")); // Provide a dio instance
    dio.options.headers["Authorization"] =
    "Bearer $token"; // config your dio headers globally
    dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: true,
        error: true,
        compact: true));
    return dio;
  }

  static Dio getDioClientWithoutToken() {
    final dio = Dio(BaseOptions(
        baseUrl: getBaseUrl(),
        contentType: "application/json"));
    dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: true,
        error: true,
        compact: true));
    return dio;
  }

  static Future<bool> hasConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        return true;
      } else {
        Utils.showToast('Vui lòng kiểm tra kết nối internet!');
        return false;
      }
    } on SocketException catch (_) {
      print('not connected');
      Utils.showToast('Vui lòng kiểm tra kết nối internet!');
      return false;
    }
  }

}

abstract class ErrorExpired {
  void expiredTime();
}