abstract class AppTranslation {
  static Map<String, Map<String, String>> translationsKeys = {
    "en_US": enUS,
    "vi": vi,
    "cn": cn,
  };
}

final Map<String, String> enUS = {
  'greeting': 'Hello, How are you?',
  'day': "Monday"
};

final Map<String, String> vi = {
  'greeting': 'Xin chào, bạn khỏe không?',
  'day': "thứ hai"
};

final Map<String, String> cn = {
  'greeting': '你好吗？',
  'day': "周一"
};