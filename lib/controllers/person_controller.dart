import 'package:base_getx/commons/Constance.dart';
import 'package:base_getx/commons/SharedPref.dart';
import 'package:base_getx/commons/networks/NetworkUtils.dart';
import 'package:base_getx/model/Person.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';

class PersonController extends GetxController{
  var people=List<Person>().obs;
  //int get count=> people.length;

  var number=17.obs;
  RxBool show=false.obs ;

  @override
  void onInit(){
    super.onInit();
    readSharePref();
  }
  readSharePref(){
    String s=SharedPref.read(Constance.key);
    print('$s');
  }

  addPerson(Person person){
    people.add(person);
  }

  addAll(List<Person> list){
    people.addAll(list);
  }

  increase(){
    number++;
  }

  checkShow(bool b){
    show=b.obs;
  }

  getData() async {
    bool checkNetwork = await NetworkUtils.hasConnection();
    if(checkNetwork) {
      //inf.showLoading(true);
      //call api > lấy giá trị truyền vào controller
      checkShow(!show.value); //show progress
      Person person=Person(name: "Nguyen Van B",age: 20,address: "Ha Noi");
      addPerson(person);
    }
  }
}
// onInit (): Được gọi ngay sau khi widget được khởi tạo.
// onReady (): Được gọi sau khi widget được hiển thị trên màn hình.
// onClose (): Được gọi ngay trước khi bộ điều khiển bị xóa khỏi bộ nhớ.