import 'dart:async';

import 'package:get/get.dart';

class ProgressController extends GetxController{
  RxDouble count=0.0.obs;
  Timer timer;

  listenProgress(){
    timer=Timer.periodic(
      const Duration(milliseconds: 10),
          (timer) {
          if (count >= 100) {
            count=100.0.obs;
             timer.cancel();
          } else {
            count ++;
          }
          print("progress: ${count.value}");
        },
    );
  }

  resetCount(){
    count=0.0.obs;
    timer.cancel();
  }

  @override
  void onClose() {
    super.onClose();
  }
}