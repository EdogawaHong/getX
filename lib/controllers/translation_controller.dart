import 'package:base_getx/commons/AppConstant.dart';
import 'package:base_getx/model/LanguageModel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TranslationController extends GetxController {
  var selectedLanguage = Get.locale.languageCode.obs;
  List<LanguageModel> languages = AppConstant.languages();

  var languageModel;

  set changeLanguage(String lang) {
    Locale locale = new Locale(lang);
    Get.updateLocale(locale);
    selectedLanguage.value = lang;
  }

  selectLanguage(LanguageModel model) {
    languageModel = model.obs;
  }

  @override
  void onInit() {
    super.onInit();
    languageModel = languages[0].obs;
    getLanguageLocal();
  }

  getLanguageLocal() {
    print('languageCode: ${Get.locale.languageCode}');
    for (int i = 0; i < languages.length; i++) {
      if (languages[i].symbol == Get.locale.languageCode) {
        selectLanguage(languages[i]);
        break;
      }
    }
  }
}
