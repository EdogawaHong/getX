import 'package:base_getx/view/HomeView.dart';
import 'package:base_getx/view/NextScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'commons/AppTranslation.dart';
import 'commons/Constance.dart';
import 'commons/SharedPref.dart';


void main() async {
  await GetStorage.init ();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  GetStorage box = GetStorage();
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Application',
      debugShowCheckedModeBanner: false,
      translationsKeys: AppTranslation.translationsKeys,
      locale: Get.deviceLocale,
      fallbackLocale: Locale('en','US'),
      defaultTransition: Transition.fade,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
        getPages: [
          GetPage(name: '/home', page: () => HomeView()),
          GetPage(name: '/next', page: () => NextScreen()),
        ],
      home:  HomeView(),
    );
  }
}