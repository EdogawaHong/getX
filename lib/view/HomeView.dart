import 'dart:convert';

import 'package:base_getx/commons/Constance.dart';
import 'package:base_getx/commons/SharedPref.dart';
import 'package:base_getx/controllers/ProgressController.dart';
import 'package:base_getx/controllers/person_controller.dart';
import 'package:base_getx/view/NextScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/percent_indicator.dart';

class HomeView extends StatelessWidget {
  final personController = Get.put(PersonController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.teal,
      body: SafeArea(child: GetX<PersonController>(
        builder: (controller) {
          return Stack(
            children: [
              Column(
                children: [
                  Expanded(
                    child: (controller.people != null &&
                        controller.people.length > 0)
                        ? ListView.builder(
                        itemCount: controller.people.length,
                        itemBuilder: (context, index) {
                          return Card(
                            margin: const EdgeInsets.all(12),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  Text('${controller.people[index].name}'),
                                  Text('${controller.people[index].age}'),
                                  Text(
                                      '${controller.people[index].address}'),
                                ],
                              ),
                            ),
                          );
                        })
                        : Center(
                      child: Text("no people"),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: RaisedButton(
                      onPressed: () {
                        showDialog();
                      },
                      child: Text('Show dialog'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: RaisedButton(
                      onPressed: () {
                        showDialogDefault();
                      },
                      child: Text('Show default dialog'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: RaisedButton(
                      onPressed: () {
                        Get.to(() => NextScreen());
                        //Get.toNamed("/next");
                      },
                      child: Text('Go to NextScreen'),
                    ),
                  )
                ],
              ),
              (controller.show.value)
                  ? Center(
                child: CircularProgressIndicator(
                  backgroundColor: Colors.yellow,
                ),
              )
                  : Container()
            ],
          );
        },
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // Person person =
          //     Person(name: "Nguyen Van A", age: 18, address: "Ha Noi");
          // personController.addPerson(person);

          var number = personController.number;
          personController.increase();
          print("number: $number");
          personController.getData();
          SharedPref.save(Constance.key, 'hello world');
          // GetUtils.isEmail("abc@gmail.com")?print('email: true'):print('email: false');
          // (GetPlatform.isIOS)?print('GetPlatform: true'):print('GetPlatform: false');
        },
        backgroundColor: Colors.amber,
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
    );
  }

  ProgressController _progressController = Get.put(ProgressController());

  showDialog() {
    return Get.dialog(
      AlertDialog(
        content: Container(
            color: Colors.white,
            width: Get.width * 2 / 3,
            height: Get.height / 2,
            child: Obx(
                  () => Column(
                children: [
                  Text(
                    'dialog',
                    style: TextStyle(fontSize: 16, color: Colors.black),
                  ),
                  SizedBox(
                    width: 0,
                    height: 16,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: LinearPercentIndicator(
                      // width: ,
                      animation: true,
                      lineHeight: 20.0,
                      percent: _progressController.count.value / 100,
                      center: Text("${_progressController.count.value}%"),
                      linearStrokeCap: LinearStrokeCap.roundAll,
                      progressColor: Colors.green,
                      animationDuration: 0,
                    ),
                  ),

                  Text(
                    'value: ${_progressController.count.value}',
                    style: TextStyle(fontSize: 16, color: Colors.black),
                  ),
                  Padding(
                    padding: EdgeInsets.all(20),
                    child: RaisedButton(
                      onPressed: () {
                        _progressController.listenProgress();
                      },
                      child: Text('Run progress'),
                    ),
                  ),
                  // Padding(padding: EdgeInsets.all(20),
                  //   child: RaisedButton(
                  //     onPressed: (){
                  //       _progressController.resetCount();
                  //     },
                  //     child: Text('Reset progress'),
                  //   ),)
                ],
              ),
            )),
      ),
      barrierDismissible: true,
      name: 'hello dialog',
    ).then((value) {
      //thực thi hành động khi close dialog
      _progressController.resetCount();
      print('close dialog');
    });
  }

  showDialogDefault() {
    return Get.defaultDialog(
        title: 'Default Dialog',
        titleStyle: TextStyle(fontSize: 20, color: Colors.red),
        content: Text('this is content'),
        textConfirm: 'Confirm',
        textCancel: 'Cancel',
        barrierDismissible: true,
        onCancel: () {
          print("cancel");
        },
        onConfirm: () {
          print('confirm');
        },
        confirmTextColor: Colors.white,
        cancelTextColor: Colors.red,
        buttonColor: Colors.blue,
        // confirm: Container(
        //   color: Colors.orange,
        //   child: Text('confirm'),
        // ),
        radius: 20);
  }
}
