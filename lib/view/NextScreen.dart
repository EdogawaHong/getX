import 'package:base_getx/commons/AppConstant.dart';
import 'package:base_getx/controllers/translation_controller.dart';
import 'package:base_getx/model/LanguageModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NextScreen extends GetWidget {
  // TranslationController _controller = Get.put(TranslationController());



  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Translation'),
        ),
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              languageChooser(),
              SizedBox(
                height: 16,
              ),
              Text(
                "Device Locale :${Get.locale.languageCode}",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
              Text(
                'greeting'.tr,
                style: TextStyle(fontSize: 16),
              ),
              Text(
                'day'.tr,
                style: TextStyle(fontSize: 16),
              )
            ],
          ),
        ));
  }

  Widget languageChooser() {
    return GetX<TranslationController>(
      init: TranslationController(),
      builder: (controller) {
        //Todo("Data ko dc null")
        return DropdownButton<LanguageModel>(
            isExpanded: false,
            hint: Text('Please choose a location'),
            value: controller.languageModel.value,
            onChanged: (item) {
              controller.changeLanguage = item.symbol;
              controller.selectLanguage(item);
            },
            items: controller.languages.map((LanguageModel _language) {
              print(_language.language);
              return DropdownMenuItem<LanguageModel>(
                child: new Text(_language.language),
                value: _language,
              );
            }).toList());
      },
    );
  }
}
